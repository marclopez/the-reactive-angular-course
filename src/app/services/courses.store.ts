import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable, throwError } from "rxjs";
import { Course, sortCoursesBySeqNo } from "../model/course";
import { catchError, map, shareReplay, tap } from "rxjs/operators";
import { HttpClient } from "@angular/common/http";
import { LoadingService } from "../loading/loading.service";
import { MessagesService } from "../messages/messages.service";

@Injectable({
  providedIn: "root",
})
export class CoursesStore {
  private subject = new BehaviorSubject<Course[]>([]);

  courses$: Observable<Course[]> = this.subject.asObservable();

  constructor(
    private http: HttpClient,
    private loading: LoadingService,
    private messages: MessagesService,
  ) {
    this.loadAllCourses();
  }

  filterByCategory(category: string): Observable<Course[]> {
    return this.courses$.pipe(
      map((courses) =>
        courses
          .filter((course) => course.category == category)
          .sort(sortCoursesBySeqNo),
      ),
    );
  }

  saveCourse(courseId: string, changes: Partial<Course>): Observable<any> {
    const courses = this.subject.getValue();
    const index = courses.findIndex((course) => course.id === courseId);
    const updatedCourse = {
      ...courses[index],
      ...changes,
    };
    const updatedCourses = courses.slice(0);
    updatedCourses[index] = updatedCourse;
    this.subject.next(updatedCourses);

    return this.http.put(`/api/courses/${courseId}`, changes).pipe(
      shareReplay(),
      catchError((err) => {
        const message = "Could not save course";
        this.messages.showErrors(message);
        return throwError(err);
      }),
    );
  }

  private loadAllCourses() {
    const loadCourses$ = this.http.get<Course[]>("/api/courses").pipe(
      map((response) => response["payload"]),
      catchError((err) => {
        const msg = "Could not load courses";
        this.messages.showErrors(msg);
        return throwError(() => err);
      }),
      tap((courses) => this.subject.next(courses)),
    );
    this.loading.showLoaderUntilCompleted(loadCourses$).subscribe();
  }
}
